FROM ubuntu:latest

# Set environment variables
ENV ANDROID_HOME /opt/android-sdk
ENV PATH $PATH:$ANDROID_HOME/tools/bin
ENV NODE_VERSION 14.x

# Install necessary packages
RUN apt-get update && \
    apt-get install -y curl unzip openjdk-11-jdk wget && \
    apt-get clean

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_$NODE_VERSION | bash -
RUN apt-get install -y nodejs

# Install Android SDK
RUN wget -q https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip -O /tmp/android-sdk.zip && \
    unzip -q /tmp/android-sdk.zip -d $ANDROID_HOME && \
    rm /tmp/android-sdk.zip

# Accept Android SDK licenses
RUN yes | $ANDROID_HOME/cmdline-tools/bin/sdkmanager --licenses

# Install Appium
RUN npm install -g appium

# Set up Appium environment variables
ENV ANDROID_AVD_HOME /root/.android/avd
ENV PATH $PATH:/usr/lib/node_modules/appium/node_modules/.bin

# Expose Appium port
EXPOSE 4723

CMD ["appium"]
