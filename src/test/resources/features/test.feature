Feature: SpringBoot Request

  @Regression
  Scenario Outline: Send a valid Request to get correct response
    Given Launch app
    # Then the response will return "<response>"

    Examples: 
      | url           | response                  |
      | /             | Hello World, Spring Boot! |
      | /qaautomation | Hello QA Automation!      |
