@FitToWorkFeature @Regression
Feature: Fit To Work feature

  @Positive @FittoWorkAssignment @HAUL-776
  Scenario: As a super admin, should be able to login using valid credential
    Given Launch app
    When User do login
      | username | password |
      | bb1920   | 12345678 |
    Then User should be success login as "Driver"
    When User click "kehadiran" tab
    And User click check in
    Then User should be able to see fit to work page
    When User filled fit to work
    And User click save fit to work
    And User confirm the fit to work
    Then User should be able to see fit to work result
