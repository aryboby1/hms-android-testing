@P2HFeature @Regression
Feature: P2H feature

  @Positive @P2HSubmit @HAUL-776
  Scenario: As a driver, should be able to submit p2h
    Given Prepare driver to do fit to work approve
    Given Launch app
    When User do login
      | username | password |
      | b59119   | 12345678 |
    Then User should be success login as "Driver"
    When User click select unit button
    And User do choose unit "AEK 0087"
    Then User should see submit P2H page
    When User input cat "A" with damage "false"
    And User do click save and next button
    When User input cat "B" with damage "false"
    And User do click save and next button
    When User input cat "C" with damage "false"
    And User do click save to rom button
    Then User should be able to see Waiting approval p2h message

  @Negative @P2HSubmitCatA10%Failed @HAUL-776
  Scenario: As a driver, should be able to submit p2h but category A 10% is failed
    Given Prepare driver to do fit to work approve
    Given Launch app
    When User do login
      | username | password |
      | b59119   | 12345678 |
    Then User should be success login as "Driver"
    When User click select unit button
    And User do choose unit "AEK 0087"
    Then User should see submit P2H page
    When User input cat "A" with damage "0.1"
    And User do click save and next button
    When User input cat "B" with damage "false"
    And User do click save and next button
    When User input cat "C" with damage "false"
    Then User should be able to see lapor admin button

  @Negative @P2HSubmitCatB10%Failed @HAUL-776
  Scenario: As a driver, should be able to submit p2h but category B 10% is failed
    Given Prepare driver to do fit to work approve
    Given Launch app
    When User do login
      | username | password |
      | b59119   | 12345678 |
    Then User should be success login as "Driver"
    When User click select unit button
    And User do choose unit "AEK 0087"
    Then User should see submit P2H page
    When User input cat "A" with damage "false"
    And User do click save and next button
    When User input cat "B" with damage "0.1"
    And User do click save and next button
    When User input cat "C" with damage "false"
    Then User should be able to see lapor admin button
