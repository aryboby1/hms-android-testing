@LoginFeature @Regression
Feature: Login feature

  @Positive @ValidLoginDriver @HAUL-731
  Scenario: As a super admin, should be able to login using valid credential
    Given Launch app
    When User do login
      | username | password |
      | b13000   | 12345678 |
    Then User should be success login as "Driver"

  @Negative @inValidUsernameLogin @HAUL-731
  Scenario: As a super admin, should not be able to login using invalid username credential
    Given Launch app
    When User do login
      | username | password |
      | b130002  | 12345678 |
    Then User should be to see error login message

  @Negative @inValidPasswordLogin @HAUL-729
  Scenario: As a super admin, should not be able to login using invalid password credential
    Given Launch app
    When User do login
      | username | password |
      | b13000   | 87654321 |
    Then User should be to see error login message
