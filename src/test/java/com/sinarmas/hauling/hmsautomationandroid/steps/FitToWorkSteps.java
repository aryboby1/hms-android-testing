package com.sinarmas.hauling.hmsautomationandroid.steps;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinarmas.hauling.hmsautomationandroid.hooks.CucumberSpringContextConfiguration;
import com.sinarmas.hauling.hmsautomationandroid.page.DashboardPage;
import com.sinarmas.hauling.hmsautomationandroid.page.FitToWorkPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

@CucumberSpringContextConfiguration
@Component("com.sinarmas.hauling.hmsautomationandroid.steps.FitToWorkSteps")
public class FitToWorkSteps extends ScenarioSteps {
    @Autowired
    DashboardPage dashboardPage;

    @Autowired
    FitToWorkPage fitToWorkPage;

    @When("User click start route")
    public void User_click_start_route() {
        // Write code here that turns the phrase above into concrete actions
        dashboardPage.clickStartMoving();
    }

    @When("User click check in")
    public void User_click_check_in() {
        // Write code here that turns the phrase above into concrete actions
        dashboardPage.clickOnCheckInBtn();
    }

    @When("User click {string} tab")
    public void User_click_tab(String menu) {
        // Write code here that turns the phrase above into concrete actions
        dashboardPage.clickOnTabHome(menu);
    }

    @Then("User should be able to see fit to work page")
    public void User_should_be_able_to_see_fit_to_work_page() {
        // Write code here that turns the phrase above into concrete actions
        fitToWorkPage.validateFitToWorkPage();
    }

    @When("User filled fit to work")
    public void User_filled_fit_to_work() {
        // Write code here that turns the phrase above into concrete actions
        fitToWorkPage.filledFitToWork("Kost", true, true, 4, 4, "11", "10", true);
    }

    @When("User click save fit to work")
    public void User_click_save_fit_to_work() {
        // Write code here that turns the phrase above into concrete actions
        fitToWorkPage.clickSaveFitToWork();
    }

    @When("User confirm the fit to work")
    public void User_confirm_the_fit_to_work() {
        // Write code here that turns the phrase above into concrete actions\
        fitToWorkPage.confirmFitToWork();
    }

    @Then("User should be able to see fit to work result")
    public void User_should_be_able_to_see_fit_to_work_result() {
        // Write code here that turns the phrase above into concrete actions
        fitToWorkPage.validateResult();
    }

    @Given("Prepare driver until get question")
    public void Prepare_driver_until_get_question() {
        // Write code here that turns the phrase above into concrete actions
        Prepare_driver_to_do_fit_to_work_approve();
        // fitToWorkPage.prepareSelectUnit();
        fitToWorkPage.getP2HQuestion();
    }

    @Given("Prepare driver to do fit to work approve")
    public void Prepare_driver_to_do_fit_to_work_approve() {
        // Write code here that turns the phrase above into concrete actions
        Prepare_driver_to_do_fit_to_work();
        fitToWorkPage.getDriverList();
        fitToWorkPage.approvalFitToWork();
    }

    @Given("Prepare driver to do fit to work")
    public void Prepare_driver_to_do_fit_to_work() {
        // Write code here that turns the phrase above into concrete actions
        fitToWorkPage.prepareLoginAdmin();
        fitToWorkPage.prepareDailyAssignment("3");
        fitToWorkPage.prepareTruckAssignment("3");
        fitToWorkPage.prepareResetAllTrip();
        fitToWorkPage.prepareLoginUser();
        fitToWorkPage.prepareDriverFitToWork();
    }

    @Given("Prepare driver to do p2h submit")
    public void Prepare_driver_to_do_p_h_submit() {
        // Write code here that turns the phrase above into concrete actions
        Prepare_driver_to_do_fit_to_work_approve();
        fitToWorkPage.prepareSelectUnit();
        fitToWorkPage.getP2HQuestion();
        fitToWorkPage.submitP2HQuestion("AK");
        fitToWorkPage.adminResponseP2H(true);
    }

}
