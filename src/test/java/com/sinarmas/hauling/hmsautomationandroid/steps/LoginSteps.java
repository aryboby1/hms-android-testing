package com.sinarmas.hauling.hmsautomationandroid.steps;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinarmas.hauling.hmsautomationandroid.hooks.CucumberSpringContextConfiguration;
import com.sinarmas.hauling.hmsautomationandroid.page.DashboardPage;
import com.sinarmas.hauling.hmsautomationandroid.page.LoginPage;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.annotations.Managed;
import net.thucydides.core.steps.ScenarioSteps;

@CucumberSpringContextConfiguration
@Component("com.sinarmas.hauling.hmsautomationandroid.steps.LoginSteps")
public class LoginSteps extends ScenarioSteps {
    @Autowired
    LoginPage loginPage;

    @Autowired
    DashboardPage dashboardPage;

    @Given("Launch app")
    public void Launch_app() {
        // Write code here that turns the phrase above into concrete actions
        // loginPage.launchApp();
    }

    @Then("User should be success login as {string}")
    public void User_should_be_success_login_as(String driverName) {
        // Write code here that turns the phrase above into concrete actions
        dashboardPage.validateDashboardPage(driverName);
    }

    @When("User do login")
    public void User_do_login(DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> columns : rows) {
            loginPage.typeEmailAndPassword(columns.get("username"), columns.get("password"));
            loginPage.clickButtonLogin();
            loginPage.checkIfAlreadyLogin();
        }
    }
    
    @Then("User should be to see error login message")
    public void User_should_be_to_see_error_login_message() {
        // Write code here that turns the phrase above into concrete actions
        loginPage.validateErrorLoginMessage();
    }


}
