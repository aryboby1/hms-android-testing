package com.sinarmas.hauling.hmsautomationandroid.steps;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sinarmas.hauling.hmsautomationandroid.hooks.CucumberSpringContextConfiguration;
import com.sinarmas.hauling.hmsautomationandroid.page.DashboardPage;
import com.sinarmas.hauling.hmsautomationandroid.page.LoginPage;
import com.sinarmas.hauling.hmsautomationandroid.page.P2HPage;
import com.sinarmas.hauling.hmsautomationandroid.page.SelectUnitPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.steps.ScenarioSteps;

@CucumberSpringContextConfiguration
@Component("com.sinarmas.hauling.hmsautomationandroid.steps.LoginSteps")
public class P2HSteps extends ScenarioSteps {

    @Autowired
    DashboardPage dashboardPage;
    @Autowired
    SelectUnitPage selectUnitPage;
    @Autowired
    P2HPage p2hPage;

    @When("User click select unit button")
    public void User_click_select_unit_button() {
        // Write code here that turns the phrase above into concrete actions
        dashboardPage.clickSelectUnitBtn();
        selectUnitPage.validateSelectUnitPage();
    }

    @When("User do choose unit {string}")
    public void User_do_choose_unit(String s) {
        // Write code here that turns the phrase above into concrete actions
        selectUnitPage.doScrollUntilFoundUnit(s);
    }

    @Then("User should see submit P2H page")
    public void User_should_see_submit_P_H_page() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.validateP2HPage();
    }

    @When("User input cat {string} with damage {string}")
    public void User_input_cat_with_damage(String s, String s2) {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.inputCatWithValue(s, s2);
    }

    @Then("User should be able to see Waiting approval p2h message")
    public void User_should_be_able_to_see_Waiting_approval_p_h_message() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.validateWaitingApprovalP2HPage();
    }

    @When("User do click save and next button")
    public void User_do_click_save_and_next_button() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.clickSaveNextButton();
    }

    @When("User do click save to rom button")
    public void User_do_click_save_to_rom_button() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.clickSaveToRomButton();
    }

    @Then("User should be able to see change unit truck")
    public void User_should_be_able_to_see_change_unit_truck() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.validateChangeUnitTruckMessage();
    }

    @Then("User should be able to see lapor admin button")
    public void User_should_be_able_to_see_lapor_admin_button() {
        // Write code here that turns the phrase above into concrete actions
        p2hPage.validateLaporAdminButton();
    }

}
