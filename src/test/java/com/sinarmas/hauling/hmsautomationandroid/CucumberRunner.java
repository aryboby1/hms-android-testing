package com.sinarmas.hauling.hmsautomationandroid;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/", stepNotifications = true, plugin = {
        "pretty",
        "json:target/destination/cucumber.json",
        "html:target/destination/cucumber-reports.html" }, 
        glue = "com.sinarmas.hauling.hmsautomationandroid.steps", 
        tags = "@Regression")
public class CucumberRunner {
static AppiumDriverLocalService appiumService = null;
    @BeforeClass
    public static void startAppium() {
        appiumService = AppiumDriverLocalService.buildDefaultService();
        appiumService.start();
    }

    @AfterClass
    public static void stopAppium() {
        appiumService.stop();
    }
}
