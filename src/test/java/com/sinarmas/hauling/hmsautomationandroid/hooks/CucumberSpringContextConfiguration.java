package com.sinarmas.hauling.hmsautomationandroid.hooks;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import com.sinarmas.hauling.hmsautomationandroid.HmsAutomationAndroidApplication;

/**
 * Class to use spring application context while running cucumber
 */

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@ContextConfiguration(classes = { HmsAutomationAndroidApplication.class })
@SpringBootTest(webEnvironment = WebEnvironment.NONE)
@TestPropertySource("classpath:/application.properties")
@PropertySource("classpath:/serenity.properties")
public @interface CucumberSpringContextConfiguration {

}
