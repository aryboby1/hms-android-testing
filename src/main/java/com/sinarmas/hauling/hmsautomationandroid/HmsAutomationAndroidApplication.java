package com.sinarmas.hauling.hmsautomationandroid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HmsAutomationAndroidApplication {

	public static void main(String[] args) {
		SpringApplication.run(HmsAutomationAndroidApplication.class, args);
	}

}
