package com.sinarmas.hauling.hmsautomationandroid.api;

import static org.hamcrest.Matchers.equalTo;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sinarmas.hauling.hmsautomationandroid.data.UrlConstant;
import com.sinarmas.hauling.hmsautomationandroid.data.WordingConstant;
import com.sinarmas.hauling.hmsautomationandroid.model.FitToWork.ResponseFitToWork;
import com.sinarmas.hauling.hmsautomationandroid.model.UnitList.ResponseUnitList;
import com.sinarmas.hauling.hmsautomationandroid.model.driverList.ResponseDriverList;
import com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment.ResponseListDailyAssignment;
import com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment.ResponseListTruckAssignment;
import com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion.ResponseP2HQuestion;
import com.sinarmas.hauling.hmsautomationandroid.util.Utility;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.questions.page.Cookies;
import io.restassured.RestAssured;
import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.MultiPartSpecification;
import io.restassured.specification.RequestSpecification;

public class WorkPreparationController {
        private static final String URL = UrlConstant.baseUrl;
        public Response response;

        @Autowired
        Utility utility = new Utility();

        public String getTokenLoginUser(String username, String password, String deviceId) {

                // We can add Key - Value pairs using the put method
                JSONObject requestParams = new JSONObject();
                requestParams.put("username", username);
                requestParams.put("password", password);
                requestParams.put("device_id", deviceId);

                response = SerenityRest
                                .given()
                                .contentType("application/json")
                                .header("Content-Type", "application/json")
                                .body(requestParams.toString())
                                .when()
                                .post(URL + UrlConstant.getApiPath("LOGIN"));

                SerenityRest.restAssuredThat(response -> response.statusCode(200));

                JSONObject jsonResponse = new JSONObject(response.getBody().asString());
                JSONObject jsonData = jsonResponse.getJSONObject("data");

                return jsonData.getString("access_token");
        }

        public String getTokenLoginAdmin(String username, String password, String deviceId) {

                // We can add Key - Value pairs using the put method
                JSONObject requestParams = new JSONObject();
                requestParams.put("username", username);
                requestParams.put("password", password);
                requestParams.put("device_id", deviceId);

                response = SerenityRest
                                .given()
                                .contentType("application/json")
                                .header("Content-Type", "application/json")
                                .body(requestParams.toString())
                                .when()
                                .post(URL + UrlConstant.getApiPath("LOGIN_ADMIN"));

                SerenityRest.restAssuredThat(response -> response.statusCode(200));

                JSONObject jsonResponse = new JSONObject(response.getBody().asString());
                JSONObject jsonData = jsonResponse.getJSONObject("data");

                return jsonData.getString("access_token");
        }

        public void resetAllTrip(String token) {

                response = SerenityRest
                                .given()
                                .contentType("application/json")
                                .header(
                                                "Content-Type", "application/json",
                                                "Authorization", "Bearer  " + token)
                                .when()
                                .get(URL + UrlConstant.getApiPath("Reset_ALL_TRIP"));

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public void getFitToWorkQuestion(String accessTokenUser) {
                response = SerenityRest
                                .given()
                                .contentType("application/json")
                                .header(
                                                "Content-Type", "application/json",
                                                "Authorization", "Bearer  " + accessTokenUser)
                                .when()
                                .get(URL + UrlConstant.getApiPath("GET_QUESTION_FIT_TO_WORK"));

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public ResponseFitToWork fitToWorkDriver(String accessTokenUser, String isafeNumber) {
                String jsonBody = "{\n" +
                                "    \"answers\": [\n" +
                                "        {\n" +
                                "            \"quetion_id\": 1,\n" +
                                "            \"answer\": \"3\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 2,\n" +
                                "            \"answer\": \"Kost/Kontrakan\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 3,\n" +
                                "            \"answer\": \"Tidak\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 4,\n" +
                                "            \"answer\": \"Tidak\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 5,\n" +
                                "            \"answer\": \"12\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 6,\n" +
                                "            \"answer\": \"13\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 7,\n" +
                                "            \"answer\": \"06:00\"\n" +
                                "        },\n" +
                                "        {\n" +
                                "            \"quetion_id\": 8,\n" +
                                "            \"answer\": \"Ya\"\n" +
                                "        }\n" +
                                "    ],\n" +
                                "    \"isafe_number\": \"" + isafeNumber + "\",\n" +
                                "    \"longitude\": -7.003836,\n" +
                                "    \"latitude\": 107.611330,\n" +
                                "    \"bypass_location_check\": true\n" +
                                "}";

                String auth = "Bearer " + accessTokenUser;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("SEND_FIT_TO_WORK"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseFitToWork.class);
        }

        public ResponseDriverList getDriverList(String accessTokenUser, String driverName,
                        String attendanceApprovalStatus) {
                String jsonBody = "{\n" +
                                "  \"filter\": {\n" +
                                // " \"driverName\": \"" + driverName + "\",\n" +
                                "    \"attendanceApprovalStatus\": \"" + attendanceApprovalStatus + "\",\n" +
                                "    \"tripStatus\": \"1\"\n" +
                                "  },\n" +
                                "  \"limit\": 10,\n" +
                                "  \"page\": 1\n" +
                                "}";

                String auth = "Bearer " + accessTokenUser;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("GET_DRIVER_LIST"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseDriverList.class);
        }

        public void approveFitToWork(String accessTokenUser, Boolean isApprove,
                        String reason, Integer tripId) {
                String jsonBody = "{\n" +
                                "  \"trip_id\": " + tripId + ",\n" +
                                "  \"is_approve\": " + String.valueOf(isApprove) + ",\n" +
                                "  \"reason\": \"" + reason + "\"\n" +
                                "}";

                String auth = "Bearer " + accessTokenUser;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("APPROVAL_FIT_TO_WORK"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public ResponseUnitList getUnitList(String accessTokenUser, Integer shiftId) {
                String jsonBody = "{\n" +
                                "  \"filter\": {\n" +
                                "            \"shiftId\": " + shiftId + "\n" +
                                "  },\n" +
                                "  \"limit\": 10,\n" +
                                "  \"page\": 1\n" +
                                "}";

                String auth = "Bearer " + accessTokenUser;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("GET_UNIT_LIST"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseUnitList.class);
        }

        public void selectUnit(String accessTokenUser, String no_lambung, Integer reference_id, String isafe_number) {
                String jsonBody = "{\"no_lambung\":\"" + no_lambung + "\",\"qr_content\":\"\"," +
                                "\"reference_id\":" + reference_id + "," +
                                "\"isafe_number\":\"" + isafe_number + "\"}";

                String auth = "Bearer " + accessTokenUser;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("SELECT_UNIT"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public ResponseP2HQuestion getP2HQuestion(String accessTokenAdmin) {
                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .when()
                                .get(URL + UrlConstant.getApiPath("GET_P2H_QUESTION"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseP2HQuestion.class);
        }

        public void submitP2HQuestion(String accessTokenUser, ResponseP2HQuestion responseP2HQuestion,
                        String damagedCat, Integer refferenceId) {

                RequestSpecification reqSpec = SerenityRest.given();
                reqSpec.multiPart("refferenceId", refferenceId);
                responseP2HQuestion.data.quetions.stream().forEach(item -> {
                        Boolean value = damagedCat.equals(item.category) ? false : true;
                        reqSpec.multiPart("answer" + item.category, value);
                });
                String auth = "Bearer " + accessTokenUser;
                response = reqSpec
                                .header("Accept", "application/json")
                                .header("Content-type", "multipart/form-data")
                                .header("Authorization", auth)
                                .when()
                                .post(URL + UrlConstant.getApiPath("SUBMIT_P2H_QUESTION"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public void createDailyAssignment(String accessTokenAdmin, Date date, String shiftOpt) {
                String dateReq = utility.dateToString(date, "yyyy-MM-dd", new Locale("id", "ID"));
                String jsonBody = "{\n" +
                                "  \"date\": \"" + dateReq + "\",\n" +
                                "  \"shift_id\": " + shiftOpt + ",\n" +
                                "  \"notes\": \"Automate daily assignment\",\n" +
                                "  \"rom_productivity\": 400,\n" +
                                "  \"pit_productivity\": 250,\n" +
                                "  \"crushing_plant_number\": 35,\n" +
                                "  \"supervisory_name\": \"Automate SPV\",\n" +
                                "  \"is_template\": false,\n" +
                                "  \"barges_number\": [\n" +
                                "    {\n" +
                                "      \"key\": \"string\",\n" +
                                "      \"name\": \"string\",\n" +
                                "      \"value\": \"string\",\n" +
                                "      \"description\": \"string\"\n" +
                                "    }\n" +
                                "  ],\n" +
                                "  \"daily_limits\": [\n" +
                                "    {\n" +
                                "      \"id\": 0,\n" +
                                "      \"type\": \"rom\",\n" +
                                "      \"route_id\": 250,\n" +
                                "      \"fleet_quota\": 30,\n" +
                                "      \"rom_quota\": 0,\n" +
                                "      \"pit_stock_origin_id\": 15,\n" +
                                "      \"truck_limits\": [\n" +
                                "        {\n" +
                                "          \"workPartnerId\": 167,\n" +
                                "          \"noQuota\": false,\n" +
                                "          \"truckType\": [\n" +
                                "            {\n" +
                                "              \"truckTypeId\": 21,\n" +
                                "              \"quantity\": 122\n" +
                                "            }\n" +
                                "          ]\n" +
                                "        }\n" +
                                "      ],\n" +
                                "      \"fleet_limits\": [\n" +
                                "        {\n" +
                                "          \"workPartnerId\": 170,\n" +
                                "          \"quantity\": 30\n" +
                                "        }\n" +
                                "      ]\n" +
                                "    }\n" +
                                "  ]\n" +
                                "}";

                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("SUBMIT_DAILY_ASSIGNMENT"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public ResponseListDailyAssignment getListDailyAssignment(String accessTokenAdmin, Date date, String shiftOpt) {
                String dateReq = utility.dateToString(date, "yyyy-MM-dd", new Locale("id", "ID"));
                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .when()
                                .queryParam("date", dateReq)
                                .queryParam("shift_id", shiftOpt)
                                .get(URL + UrlConstant.getApiPath("GET_DAILY_ASSIGNMENT"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseListDailyAssignment.class);
        }

        public ResponseListTruckAssignment getListTruckAssignment(String accessTokenAdmin, Date date, String shiftOpt) {
                String dateReq = utility.dateToString(date, "yyyy-MM-dd", new Locale("id", "ID"));
                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .when()
                                .queryParam("date", dateReq)
                                .queryParam("shift_id", shiftOpt)
                                .get(URL + UrlConstant.getApiPath("GET_Truck_ASSIGNMENT"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
                return response.getBody().as(ResponseListTruckAssignment.class);
        }

        public void createTruckAssignment(String accessTokenAdmin, Integer dailyLimitId, Date date, String shiftOpt,
                        String routeId) {
                String dateReq = utility.dateToString(date, "yyyy-MM-dd", new Locale("id", "ID"));
                String jsonBody = WordingConstant.truckAssignmentReq(dailyLimitId, dateReq, shiftOpt, routeId);

                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("SUBMIT_TRUCK_ASSIGNMENT"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }

        public void responseP2H(String accessTokenAdmin, Integer reference_id, boolean isApprove,
                        String randomAlphanumeric) {

                String jsonBody = WordingConstant.approveP2HReq(reference_id, isApprove, randomAlphanumeric);

                String auth = "Bearer " + accessTokenAdmin;
                response = SerenityRest
                                .given()
                                .contentType("application/json; charset=utf-8")
                                .header("Authorization", auth)
                                .header("Content-Type", "application/json; charset=utf-8")
                                .body(utility.generateJsonNode(jsonBody))
                                .when()
                                .post(URL + UrlConstant.getApiPath("RESPONSE_P2H"));

                response.prettyPrint();

                SerenityRest.restAssuredThat(
                                response -> response.statusCode(200));
        }
}
