package com.sinarmas.hauling.hmsautomationandroid.page;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class DashboardPage extends BasePage {

    // Web Element
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Selamat Datang\"]")
    private WebElement welcomeTitleDashboard;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"DIMAS [PROD] SUTEJO\"]")
    private WebElement driverNameTXT;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Kehadiran\"]/parent::android.view.View")
    private WebElement tabKehadiran;
    @AndroidFindBy(xpath = "//android.view.View[@text=\"Home\"]")
    private WebElement tabHome;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Perjalanan\"]/parent::android.view.View")
    private WebElement tabStartMoving;

    @AndroidFindBy(xpath = "(//android.widget.TextView[@text=\"Kehadiran\"])[1]")
    private WebElement tittleKehadiranPage;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Check In\"]")
    private WebElement btnCheckIn;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Select Unit\"]")
    private WebElement btnSelectUnit;

    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public void validateDashboardPage(String driverName) {
        webWaitVisibility(welcomeTitleDashboard, false, 0);
        assertThat("Welcome dashboard Title is not visible",
                webWaitIsVisibility(welcomeTitleDashboard, 3),
                equalTo(true));
    }

    public void clickStartMoving() {
    }

    public void clickOnTabHome(String menu) {
        for (int i = 0; i < 3; i++) {
            printLOG("clickOnTabHome", i);
            switch (menu) {
                case "kehadiran":
                    clickOnBtn(tabKehadiran);
                    break;
                case "Home":
                    clickOnBtn(tabHome);
                    break;
                case "Route":
                    clickOnBtn(tabStartMoving);
                    break;
            }
            if (menu.equalsIgnoreCase("kehadiran") && webWaitIsVisibility(tittleKehadiranPage, 2)) {

                printLOG("kehadiran", i);
                break;
            } else if (menu.equalsIgnoreCase("Home") && webWaitIsVisibility(tittleKehadiranPage, 2)) {
                break;
            } else if (menu.equalsIgnoreCase("Route") && webWaitIsVisibility(tittleKehadiranPage, 2)) {
                break;
            }

            printLOG("clickOnTabHome", i);
        }
    }

    public void clickOnCheckInBtn() {
        clickOnBtn(btnCheckIn);
    }

    public void clickSelectUnitBtn() {
        clickOnBtn(btnSelectUnit);
    }
}
