package com.sinarmas.hauling.hmsautomationandroid.page;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class LoginPage extends BasePage {

    // Web Element
    @AndroidFindBy(xpath = "//android.widget.EditText[@resource-id=\"ion-input-0\"]")
    private WebElement isafeNumberField;
    @AndroidFindBy(xpath = "//android.widget.EditText[@resource-id=\"ion-input-1\"]")
    private WebElement passwordField;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Masuk\"]")
    private WebElement loginButton;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Akun anda telah login di perangkat lain\"]")
    private WebElement alreadyLoginTXT;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Ya\"]")
    private WebElement yaConfirmBtn;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Id atau password tidak valid\"]")
    private WebElement errorLoginTXT;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void typeEmailAndPassword(String isafeNumber, String password) {
        webWaitVisibility(isafeNumberField, true, 3);
        element(isafeNumberField).type(isafeNumber);
        element(passwordField).type(password);
    }

    public void clickButtonLogin() {
        clickOnBtn(loginButton);
    }

    public void checkIfAlreadyLogin() {
        if (webWaitIsVisibility(alreadyLoginTXT, 3)) {
            element(yaConfirmBtn).click();
        }
    }

    public void validateErrorLoginMessage() {
        webWaitVisibility(errorLoginTXT, false, 0);
        assertThat("Welcome dashboard Title is not visible",
                webWaitIsVisibility(errorLoginTXT, 3),
                equalTo(true));
    }

}
