package com.sinarmas.hauling.hmsautomationandroid.page;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class SelectUnitPage extends BasePage {
    
    // Locator
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Silakan pilih unit\"]")
    private WebElement selectUnitTitlePage;
    @AndroidFindBy(xpath = "//android.widget.Image[@text=\"icon-dump-truck\"]")
    private WebElement iconDumpTruck;
    private String unitText = "new UiSelector().text(\"%s\")";
    private String scrollAbleElement = "new UiSelector().resourceId(\"background-content\")";
    public SelectUnitPage(WebDriver driver) {
        super(driver);
    }

    public void validateSelectUnitPage() {
        assertThat("Select Unit Title is not visible",
                webWaitIsVisibility(selectUnitTitlePage, 3),
                equalTo(true));
    }

    public void doScrollUntilFoundUnit(String s) {
        webWaitVisibility(iconDumpTruck, true, 5);
        scrollUntilElementFound(scrollAbleElement, String.format(unitText, s));
        clickOnBtn(getElementUIAutomator(String.format(unitText, s)));
    }
}
