package com.sinarmas.hauling.hmsautomationandroid.page;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.ImmutableMap;

import io.appium.java_client.AppiumBy.ByAndroidUIAutomator;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import net.serenitybdd.core.annotations.findby.By;

public class BasePage extends MobilePageObject {
    public BasePage(WebDriver driver) {
        super(driver);
    }

    public void webWaitVisibility(WebElement element, boolean visible, int timeout) {
        Duration durationInMinutes = Duration.ofSeconds(timeout);
        WebDriverWait wait = new WebDriverWait(getDriver(), durationInMinutes);
        try {
            if (visible) {
                wait.until(ExpectedConditions.visibilityOf(element));
            } else {
                wait.until(ExpectedConditions.invisibilityOf(element));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void webWaitClickable(WebElement element, int timeout) {
        Duration durationInMinutes = Duration.ofSeconds(timeout);
        WebDriverWait wait = new WebDriverWait(getDriver(), durationInMinutes);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public Boolean webWaitIsVisibility(WebElement element, int timeout) {
        Boolean result = false;
        try {
            webWaitVisibility(element, true, timeout);
            result = isVisible(element);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean isVisible(WebElement element) {
        return element(element).isVisible();
    }

    public void launchApp() {
        getDriver();
        getDriver().getPageSource();
    }

    public void clickOnBtn(WebElement element) {
        webWaitClickable(element, 3);
        element(element).click();
    }

    public void clickOnRadioBtn(WebElement element) {
        webWaitClickable(element, 3);
        for (int i = 0; i < 3; i++) {
            if (element.getAttribute("checked").equalsIgnoreCase("false")) {
                element(element).click();
            } else {
                break;
            }
        }
    }

    public String getText(WebElement element) {
        webWaitVisibility(element, true, 3);
        return element(element).getText();
    }

    public void printLOG(String s, Object obj) {
        System.out.println("=======" + s + "=======");
        System.out.println(obj);
        System.out.println("================");
    }

    public WebElement getElementUIAutomator(String element) {
        return getDriver().findElement(ByAndroidUIAutomator.androidUIAutomator(element));
    }

    public WebElement getElementXpath(String element) {
        return getDriver().findElement(By.xpath(element));
    }

    public void scrollAndroidElement(WebElement element, String direction) {
        ((JavascriptExecutor) getDriver()).executeScript("mobile: scrollGesture", ImmutableMap.of(
                "elementId", ((RemoteWebElement) element).getId(),
                "left", 100, "top", 100, "width", 200, "height", 200,
                "direction", direction,
                "percent", 5.0));

    }

    public void scrollUntilElementFound(String parentScrollLocator, String elementFoundLocator) {
        String locator = "new UiScrollable(%s).setAsVerticalList().setSwipeDeadZonePercentage(0.3).scrollToEnd(1)";
        for (int i = 0; i < 10; i++) {
            if (checkElementVisibleOnScreenPercentage(getElementUIAutomator(elementFoundLocator), false, 0.7)) {
                break;
            } else {
                getElementUIAutomator(String.format(locator, parentScrollLocator));
            }
        }
        waitABit(2000);
    }

    public void scrollUntilElementFoundToBegin(String parentScrollLocator) {
        String locator = "new UiScrollable(%s).setAsVerticalList().setSwipeDeadZonePercentage(0.3).scrollToBeginning(10,5)";
        for (int i = 0; i < 3; i++) {
            getElementUIAutomator(String.format(locator, parentScrollLocator));
        }
        waitABit(1000);
    }

    public Boolean checkElementVisibleOnScreenPercentage(WebElement element, Boolean isX, double percentage) {
        Boolean result = false;
        if (isX) {
            double xScreen = getDriver().manage().window().getSize().width * percentage;
            Integer xElement = element.getLocation().x;
            printLOG("xScreen", String.format("%s < %s", xElement, xScreen));
            if (xElement < xScreen) {
                result = true;
            }
        } else {
            double yScreen = getDriver().manage().window().getSize().height * percentage;
            Integer yElement = element.getLocation().y;
            printLOG("yScreen", String.format("%s < %s", yElement, yScreen));
            if (yElement < yScreen) {
                result = true;
            }
        }
        printLOG("result", result);
        return result;
    }
}
