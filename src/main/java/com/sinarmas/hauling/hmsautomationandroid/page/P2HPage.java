package com.sinarmas.hauling.hmsautomationandroid.page;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinarmas.hauling.hmsautomationandroid.api.WorkPreparationController;
import com.sinarmas.hauling.hmsautomationandroid.data.CommonData;
import com.sinarmas.hauling.hmsautomationandroid.data.MasterDataData;
import com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion.Quetion;
import com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion.ResponseP2HQuestion;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class P2HPage extends BasePage {

    // Locator
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Isi Data P2H\"]")
    private WebElement p2HTitlePage;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Menunggu Approval P2H\"]")
    private WebElement waitingApprovalP2HTitlePage;

    @AndroidFindBy(uiAutomator = "new UiSelector().scrollable(true)")
    private WebElement scrollableElementFind;

    @AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc=\"Shutter\"]")
    private WebElement shutterBtn;
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Done\"]")
    private WebElement shutterDoneBtn;

    private String scrollableElement = "new UiSelector().scrollable(true)";
    private String elementQuestionText = "new UiScrollable(new UiSelector().scrollable(true)).scrollForward().setSwipeDeadZonePercentage(0.7).setMaxSearchSwipes(10).scrollIntoView(new UiSelector().text(\"%s\"))";
    private String elementFirstQuestionText = "new UiSelector().text(\"%s\")";
    private String elementCatText = "new UiSelector().textContains(\"Kategori %s\")";
    private String elementLihatRiwayatRitaseText = "new UiSelector().text(\"Lihat riwayat ritase\")";
    private String yaAnswerRb = "//android.view.View[@text=\"%s\"]/following-sibling::*[1]//android.widget.RadioButton[@text=\"Ya\"]";
    private String noAnswerRb = "//android.view.View[@text=\"%s\"]/following-sibling::*[1]//android.widget.RadioButton[@text=\"Tidak\"]";
    private String pilihFotoBtn = "//android.view.View[@text=\"%s\"]/following-sibling::*[2]//android.widget.Button[@text=\"Pilih Foto\"]";
    // private String yaAnswerRb =
    // "//android.view.View[@resource-id=\"main-content\"]/android.view.View[2]/android.view.View/android.view.View[%s]//android.widget.RadioButton[@text=\"Ya\"]";
    // private String noAnswerRb =
    // "//android.view.View[@resource-id=\"main-content\"]/android.view.View[2]/android.view.View/android.view.View[%s]//android.widget.RadioButton[@text=\"Tidak\"]";
    private String catSelectionTitle = "new UiSelector().className(\"android.view.View\").text(\"Kategori %s\")";
    private String savenextCategoryButton = "new UiSelector().className(\"android.widget.Button\").text(\"Simpan dan Lanjutkan\")";
    private String savenextToROMButton = "new UiSelector().className(\"android.widget.Button\").text(\"Simpan dan Lanjut ke ROM\")";
    private String laporAdminButton = "new UiSelector().className(\"android.widget.Button\").text(\"Lapor Admin\")";

    @Autowired
    CommonData commonData = new CommonData();

    @Autowired
    WorkPreparationController workPreparationController = new WorkPreparationController();

    @Autowired
    ResponseP2HQuestion responseP2HQuestion = new ResponseP2HQuestion();

    public P2HPage(WebDriver driver) {
        super(driver);
    }

    public void validateP2HPage() {
        assertThat("P2H Title is not visible",
                webWaitIsVisibility(p2HTitlePage, 3),
                equalTo(true));
    }

    public void validateWaitingApprovalP2HPage() {
        assertThat("Waiting approval P2H page is not visible",
                webWaitIsVisibility(waitingApprovalP2HTitlePage, 10),
                equalTo(true));
    }

    public void prepareLoginAdmin() {
        commonData.setAccessTokenAdmin(workPreparationController.getTokenLoginAdmin(
                MasterDataData.getEmailUser("AdminDwiki"),
                MasterDataData.getPassUser("AdminDwiki"),
                RandomStringUtils.randomAlphanumeric(10)));
    }

    public void getP2HQuestion() {
        prepareLoginAdmin();
        responseP2HQuestion = workPreparationController.getP2HQuestion(commonData.getAccessTokenAdmin());
    }

    public void inputCatWithValue(String s, String s2) {
        webWaitVisibility(getElementUIAutomator(String.format(catSelectionTitle, s)), true, 3);
        getP2HQuestion();
        List<Quetion> list = responseP2HQuestion.data.quetions.stream().filter(item -> item.category.equals(s))
                .collect(Collectors.toList());

        if (!webWaitIsVisibility(scrollableElementFind, 1)) {
            scrollAndroidElement(getElementUIAutomator(String.format(elementCatText, s)), "down");
        }
        scrollUntilElementFoundToBegin(scrollableElement);
        int countQuestion = 0;
        if (!s2.equalsIgnoreCase("false")) {
            countQuestion = (int) (Math.floor(list.size() * Double.parseDouble(s2)));
        }
        for (int i = 0; i < list.size(); i++) {
            scrollUntilElementFound(scrollableElement, String.format(elementQuestionText, list.get(i).quetion));
            if (s2.equalsIgnoreCase("false")) {
                clickOnRadioBtn(getElementXpath(String.format(yaAnswerRb, list.get(i).quetion)));
            } else {
                if (i < countQuestion) {
                    clickOnRadioBtn(getElementXpath(String.format(noAnswerRb, list.get(i).quetion)));
                    clickOnBtn(getElementXpath(String.format(pilihFotoBtn, list.get(i).quetion)));
                    clickOnBtn(shutterBtn);
                    clickOnBtn(shutterDoneBtn);
                    waitABit(1000);
                } else {
                    clickOnRadioBtn(getElementXpath(String.format(yaAnswerRb, list.get(i).quetion)));

                }
            }
        }
    }

    public void clickSaveNextButton() {
        scrollUntilElementFound(scrollableElement, savenextCategoryButton);
        clickOnBtn(getElementUIAutomator(savenextCategoryButton));
    }

    public void clickSaveToRomButton() {
        scrollUntilElementFound(scrollableElement, savenextToROMButton);
        clickOnBtn(getElementUIAutomator(savenextToROMButton));
    }

    public void validateChangeUnitTruckMessage() {
    }

    public void validateLaporAdminButton() {
        assertThat("Waiting approval P2H page is not visible",
                webWaitIsVisibility(getElementUIAutomator(laporAdminButton), 10),
                equalTo(true));
        
    }
}
