package com.sinarmas.hauling.hmsautomationandroid.page;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;

import com.sinarmas.hauling.hmsautomationandroid.api.WorkPreparationController;
import com.sinarmas.hauling.hmsautomationandroid.data.CommonData;
import com.sinarmas.hauling.hmsautomationandroid.data.MasterDataData;
import com.sinarmas.hauling.hmsautomationandroid.model.FitToWork.ResponseFitToWork;
import com.sinarmas.hauling.hmsautomationandroid.model.UnitList.ResponseUnitList;
import com.sinarmas.hauling.hmsautomationandroid.model.driverList.ResponseDriverList;
import com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment.ResponseListDailyAssignment;
import com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment.ResponseListTruckAssignment;
import com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion.ResponseP2HQuestion;

import io.appium.java_client.pagefactory.AndroidFindBy;

public class FitToWorkPage extends BasePage {

    // Web Element
    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Form Kesiapan Kerja\"]")
    private WebElement txtTitleFitToWorkPage;

    @AndroidFindBy(xpath = "//android.widget.Spinner[@resource-id=\"ion-sel-0\"]")
    private WebElement selectPlaceBeforeWork;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text=\"Kost/Kontrakan\"]")
    private WebElement optKost;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text=\"Rumah Tinggal/Keluarga\"]")
    private WebElement optHome;
    @AndroidFindBy(xpath = "//android.widget.RadioButton[@text=\"Lainnya\"]")
    private WebElement optOther;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"CANCEL\"]")
    private WebElement btnCancel;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"OK\"]")
    private WebElement btnOk;

    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[2]//android.widget.RadioButton[@text=\"Ya\"]")
    private WebElement rbDrugsQYes;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[2]//android.widget.RadioButton[@text=\"Tidak\"]")
    private WebElement rbDrugsQNo;

    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[3]//android.widget.RadioButton[@text=\"Ya\"]")
    private WebElement rbConsentQYes;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[3]//android.widget.RadioButton[@text=\"Tidak\"]")
    private WebElement rbConsentQNo;

    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[7]//android.widget.RadioButton[@text=\"Ya\"]")
    private WebElement rbReadyWorkQYes;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[7]//android.widget.RadioButton[@text=\"Tidak\"]")
    private WebElement rbReadyWorkQNo;

    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[4]//android.widget.TextView[1]")
    private WebElement txtOneDBeforeWork;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[4]/android.view.View[1]")
    private WebElement btnDecOneDBeforeWork;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[4]/android.view.View[2]")
    private WebElement btnInsOneDBeforeWork;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[5]//android.widget.TextView[1]")
    private WebElement txtTwoDBeforeWork;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[5]/android.view.View[1]")
    private WebElement btnDecTwoDBeforeWork;
    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[5]/android.view.View[2]")
    private WebElement btnInsTwoDBeforeWork;

    @AndroidFindBy(xpath = "//android.view.View[@resource-id=\"kesiapan-form\"]/android.view.View[6]")
    private WebElement inputTimeWakeUp;
    @AndroidFindBy(xpath = "//android.widget.EditText[contains(@resource-id,\"hour\")]")
    private WebElement inputHourTimeWakeUp;
    @AndroidFindBy(xpath = "//android.widget.EditText[contains(@resource-id,\"minute\")]")
    private WebElement inputMinuteTimeWakeUp;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"OK\"]")
    private WebElement btnSaveTimeWakeUp;

    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Simpan dan Lanjutkan\"]")
    private WebElement btnSaveAndContinue;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Data yang dimasukkan adalah benar!\"]")
    private WebElement titleConfirmFitToWork;
    @AndroidFindBy(xpath = "//android.widget.CheckBox[@resource-id=\"ion-cb-0\"]")
    private WebElement cbConfirm;
    @AndroidFindBy(xpath = "//android.widget.Button[@text=\"Lanjutkan\"]")
    private WebElement btnOkConfirm;

    @Autowired
    CommonData commonData = new CommonData();

    @Autowired
    WorkPreparationController workPreparationController = new WorkPreparationController();

    @Autowired
    ResponseDriverList response = new ResponseDriverList();

    @Autowired
    ResponseUnitList responseUnitList = new ResponseUnitList();

    @Autowired
    ResponseFitToWork responseFitToWork = new ResponseFitToWork();

    @Autowired
    ResponseP2HQuestion responseP2HQuestion = new ResponseP2HQuestion();

    @Autowired
    ResponseListDailyAssignment responseListDailyAssignment = new ResponseListDailyAssignment();

    @Autowired
    ResponseListTruckAssignment responseListTruckAssignment = new ResponseListTruckAssignment();

    public FitToWorkPage(WebDriver driver) {
        super(driver);
    }

    public void validateFitToWorkPage() {
        webWaitVisibility(txtTitleFitToWorkPage, false, 3);
        assertThat("Fit to work page Title is not visible",
                webWaitIsVisibility(txtTitleFitToWorkPage, 3),
                equalTo(true));
    }

    public void filledFitToWork(String placeRest, boolean isConsumeDrug, boolean isPersonalIssue, int oneDaySleep,
            int twoDaySleep, String hourTimeWakeUp, String minuteTimeWakeUp, boolean isReadyForWork) {
        if (isConsumeDrug) {
            clickOnBtn(rbDrugsQYes);
        } else {
            clickOnBtn(rbDrugsQNo);
        }

        if (isPersonalIssue) {
            clickOnBtn(rbConsentQYes);
        } else {
            clickOnBtn(rbConsentQNo);
        }

        if (isReadyForWork) {
            clickOnBtn(rbReadyWorkQYes);
        } else {
            clickOnBtn(rbReadyWorkQNo);
        }

        clickOnBtn(selectPlaceBeforeWork);
        switch (placeRest) {
            case "Kost":
                clickOnBtn(optKost);
                break;
            case "Rumah":
                clickOnBtn(optHome);
                break;
            case "Other":
                clickOnBtn(optOther);
                break;
        }
        clickOnBtn(btnOk);

        int actOneDaySleep = Integer.parseInt(getText(txtOneDBeforeWork));
        printLOG("oneDaySleep actOneDaySleep", oneDaySleep + "  " + actOneDaySleep);
        while (oneDaySleep != actOneDaySleep) {
            printLOG("oneDaySleep actOneDaySleep", oneDaySleep + "  " + actOneDaySleep);
            if (oneDaySleep < actOneDaySleep) {
                clickOnBtn(btnDecOneDBeforeWork);
            } else {
                clickOnBtn(btnInsOneDBeforeWork);
            }
            actOneDaySleep = Integer.parseInt(getText(txtOneDBeforeWork));

            printLOG("oneDaySleep actOneDaySleep", oneDaySleep + "  " + actOneDaySleep);
        }
        int actTwoDaySleep = Integer.parseInt(getText(txtTwoDBeforeWork));
        printLOG("twoDaySleep actTwoDaySleep", twoDaySleep + "  " + actTwoDaySleep);
        while (twoDaySleep != actTwoDaySleep) {
            printLOG("twoDaySleep actTwoDaySleep", twoDaySleep + "  " + actTwoDaySleep);
            if (twoDaySleep < actTwoDaySleep) {
                clickOnBtn(btnDecTwoDBeforeWork);
            } else {
                clickOnBtn(btnInsTwoDBeforeWork);
            }
            actTwoDaySleep = Integer.parseInt(getText(txtOneDBeforeWork));
            printLOG("twoDaySleep actTwoDaySleep", twoDaySleep + "  " + actTwoDaySleep);
        }
        clickOnBtn(inputTimeWakeUp);
        typeInto(inputHourTimeWakeUp, hourTimeWakeUp);
        typeInto(inputMinuteTimeWakeUp, minuteTimeWakeUp);
        clickOnBtn(btnSaveTimeWakeUp);

    }

    public void clickSaveFitToWork() {
        clickOnBtn(btnSaveAndContinue);
    }

    public void confirmFitToWork() {
        webWaitVisibility(titleConfirmFitToWork, false, 3);
        assertThat("Fit to work confirm Title is not visible",
                webWaitIsVisibility(titleConfirmFitToWork, 3),
                equalTo(true));
        clickOnBtn(cbConfirm);
        clickOnBtn(btnOkConfirm);
    }

    public void validateResult() {
        webWaitVisibility(titleConfirmFitToWork, false, 3);
        assertThat("Fit to work confirm Title is not visible",
                webWaitIsVisibility(titleConfirmFitToWork, 3),
                equalTo(true));
    }

    public void prepareLoginAdmin() {
        commonData.setAccessTokenAdmin(workPreparationController.getTokenLoginAdmin(
                MasterDataData.getEmailUser("AdminDwiki"),
                MasterDataData.getPassUser("AdminDwiki"),
                RandomStringUtils.randomAlphanumeric(10)));
    }

    public void prepareDailyAssignment(String shiftOpt) {
        Date date = new Date();
        // date = utility.addDateDay(date, 1);
        responseListDailyAssignment = workPreparationController.getListDailyAssignment(
                commonData.getAccessTokenAdmin(),
                date, shiftOpt);
        if (responseListDailyAssignment.data.dailyLimits.size() == 0) {
            workPreparationController.createDailyAssignment(
                    commonData.getAccessTokenAdmin(),
                    date, shiftOpt);
        }
    }

    public void prepareTruckAssignment(String shiftOpt) {
        Date date = new Date();

        responseListTruckAssignment = workPreparationController.getListTruckAssignment(
                commonData.getAccessTokenAdmin(),
                date, shiftOpt);

        responseListTruckAssignment.data.detailResponses.stream().forEach(item -> {
            workPreparationController.createTruckAssignment(
                    commonData.getAccessTokenAdmin(), item.dailyLimitId,
                    date, shiftOpt, String.valueOf(item.route.routeId));
        });

    }

    public void prepareResetAllTrip() {
        workPreparationController.resetAllTrip(commonData.getAccessTokenAdmin());
    }

    public void prepareLoginUser() {
        commonData.setAccessTokenUser(workPreparationController.getTokenLoginUser(
                MasterDataData.getEmailUser("WIRADAT"),
                MasterDataData.getPassUser(""),
                RandomStringUtils.randomAlphanumeric(10)));
    }

    public void prepareDriverFitToWork() {
        responseFitToWork = workPreparationController.fitToWorkDriver(commonData.getAccessTokenUser(),
                MasterDataData.getEmailUser("WIRADAT"));
    }

    public void getDriverList() {
        response = workPreparationController.getDriverList(
                commonData.getAccessTokenAdmin(),
                "WIRADAT",
                "SUBMITTED");
    }

    public void approvalFitToWork() {
        workPreparationController.approveFitToWork(
                commonData.getAccessTokenAdmin(),
                true,
                RandomStringUtils.randomAlphanumeric(10),
                response.data.rows.get(0).tripId);
    }

    public void prepareSelectUnit() {
        String no_lambung = "Aek 0172";
        Integer reference_id = responseFitToWork.data.referenceId;
        String isafe_number = response.data.rows.get(0).isafeNumber;
        workPreparationController.selectUnit(
                commonData.getAccessTokenAdmin(),
                no_lambung,
                reference_id,
                isafe_number);
    }

    public void getP2HQuestion() {
        responseP2HQuestion = workPreparationController.getP2HQuestion(commonData.getAccessTokenAdmin());
    }

    public void submitP2HQuestion(String damagedCat) {
        Integer reference_id = responseFitToWork.data.referenceId;
        workPreparationController.submitP2HQuestion(
                commonData.getAccessTokenUser(),
                responseP2HQuestion,
                damagedCat,
                reference_id);
    }

    public void adminResponseP2H(boolean isApprove) {
        Integer reference_id = responseFitToWork.data.referenceId;
        workPreparationController.responseP2H(
                commonData.getAccessTokenAdmin(),
                reference_id,
                isApprove,
                RandomStringUtils.randomAlphanumeric(10));
    }

}
