package com.sinarmas.hauling.hmsautomationandroid.model.driverList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RowDriver {

    @JsonProperty("driverId")
    public Integer driverId;
    @JsonProperty("driverName")
    public String driverName;
    @JsonProperty("driverPartner")
    public Object driverPartner;
    @JsonProperty("driverPhoto")
    public String driverPhoto;
    @JsonProperty("itwsId")
    public Integer itwsId;
    @JsonProperty("isafeNumber")
    public String isafeNumber;
    @JsonProperty("tripId")
    public Integer tripId;
    @JsonProperty("tripDate")
    public String tripDate;
    @JsonProperty("totalTravelTimeToDateInMinute")
    public Integer totalTravelTimeToDateInMinute;
    @JsonProperty("totalTravelDistanceToDateInKm")
    public Object totalTravelDistanceToDateInKm;
    @JsonProperty("averageSpeed")
    public Object averageSpeed;
    @JsonProperty("kontraktor")
    public String kontraktor;
    @JsonProperty("birthDate")
    public String birthDate;
    @JsonProperty("lastTripTime")
    public String lastTripTime;
    @JsonProperty("currentShift")
    public Object currentShift;
    @JsonProperty("currentShiftDescription")
    public Object currentShiftDescription;
    @JsonProperty("shiftDate")
    public Object shiftDate;
    @JsonProperty("workPreparationStatus")
    public String workPreparationStatus;
    @JsonProperty("tripStatus")
    public Integer tripStatus;
    @JsonProperty("tripStatusDescription")
    public String tripStatusDescription;
    @JsonProperty("attendanceApprovalStatus")
    public String attendanceApprovalStatus;
    @JsonProperty("p2hApprovalStatus")
    public Object p2hApprovalStatus;
    @JsonProperty("releaseRequestorDate")
    public Object releaseRequestorDate;
    @JsonProperty("releaseRequestorStatus")
    public Object releaseRequestorStatus;
    @JsonProperty("releaseRequestorUserId")
    public Object releaseRequestorUserId;
    @JsonProperty("releaseRequestorUserName")
    public Object releaseRequestorUserName;
    @JsonProperty("rom")
    public Object rom;
    @JsonProperty("portName")
    public Object portName;
    @JsonProperty("routeId")
    public Object routeId;
    @JsonProperty("noLambung")
    public Object noLambung;
    @JsonProperty("truckTypeName")
    public Object truckTypeName;
    @JsonProperty("totalRitase")
    public Integer totalRitase;
    @JsonProperty("sos")
    public Object sos;
    @JsonProperty("violationHistories")
    public Object violationHistories;
}
