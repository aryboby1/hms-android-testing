package com.sinarmas.hauling.hmsautomationandroid.model.UnitList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DataUnit {

@JsonProperty("total_row")
public Integer totalRow;
@JsonProperty("total_page")
public Integer totalPage;
@JsonProperty("rows")
public List<RowUnit> rows;
}
