package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Truck {

    @JsonProperty("truckTypeId")
    public Integer truckTypeId;
    @JsonProperty("name")
    public String name;
    @JsonProperty("quantity")
    public Integer quantity;
    @JsonProperty("trucks")
    public List<Truck__1> trucks;
}
