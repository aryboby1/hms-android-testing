package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class WorkPartner {

    @JsonProperty("work_partner_id")
    public Integer workPartnerId;
    @JsonProperty("code")
    public String code;
    @JsonProperty("name")
    public String name;
}
