package com.sinarmas.hauling.hmsautomationandroid.model.driverList;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ibm.icu.impl.Row;

import lombok.Data;

@Data
public class DataDriver {
    @JsonProperty("total_row")
    public Integer totalRow;
    @JsonProperty("total_page")
    public Integer totalPage;
    @JsonProperty("rows")
    public List<RowDriver> rows;
}
