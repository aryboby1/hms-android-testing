package com.sinarmas.hauling.hmsautomationandroid.model.FitToWork;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DataFitToWork {

    @JsonProperty("attendance_date")
    public String attendanceDate;
    @JsonProperty("reference_id")
    public Integer referenceId;
    @JsonProperty("score")
    public Integer score;
    @JsonProperty("score_description")
    public String scoreDescription;
}
