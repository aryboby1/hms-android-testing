package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ShiftResponse {

    @JsonProperty("id")
    public Integer id;
    @JsonProperty("code")
    public Integer code;
    @JsonProperty("name")
    public String name;
    @JsonProperty("start_shift")
    public String startShift;
    @JsonProperty("end_shift")
    public String endShift;
    @JsonProperty("checkin_time_start")
    public Object checkinTimeStart;
    @JsonProperty("checkin_time_end")
    public String checkinTimeEnd;
    @JsonProperty("description")
    public String description;
}
