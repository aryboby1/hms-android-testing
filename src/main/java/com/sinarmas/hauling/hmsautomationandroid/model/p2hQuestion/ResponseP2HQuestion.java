package com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseP2HQuestion {

    @JsonProperty("code")
    public String code;
    @JsonProperty("message")
    public String message;
    @JsonProperty("data")
    public DataP2HQuestion data;
}
