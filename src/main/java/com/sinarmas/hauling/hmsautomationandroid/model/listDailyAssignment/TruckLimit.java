package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TruckLimit  {

    @JsonProperty("workPartner")
    public WorkPartner workPartner;
    @JsonProperty("noQuota")
    public Boolean noQuota;
    @JsonProperty("truckType")
    public List<TruckType> truckType;
}
