package com.sinarmas.hauling.hmsautomationandroid.model.FitToWork;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseFitToWork {

    @JsonProperty("code")
    public String code;
    @JsonProperty("message")
    public String message;
    @JsonProperty("data")
    public DataFitToWork data;
}
