package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseListTruckAssignment {

    @JsonProperty("code")
    public String code;
    @JsonProperty("message")
    public String message;
    @JsonProperty("data")
    public DataListTruckAssignment data;
}
