package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class FleetLimit  {

    @JsonProperty("workPartner")
    public WorkPartner workPartner;
    @JsonProperty("quantity")
    public Integer quantity;
    @JsonProperty("code")
    public String code;
}
