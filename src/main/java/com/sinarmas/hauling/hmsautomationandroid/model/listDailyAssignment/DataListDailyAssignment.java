package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DataListDailyAssignment {

    @JsonProperty("date")
    public String date;
    @JsonProperty("shift_id")
    public Integer shiftId;
    @JsonProperty("notes")
    public String notes;
    @JsonProperty("rom_productivity")
    public Float romProductivity;
    @JsonProperty("pit_productivity")
    public Float pitProductivity;
    @JsonProperty("crushing_plant_number")
    public Float crushingPlantNumber;
    @JsonProperty("supervisory_name")
    public String supervisoryName;
    @JsonProperty("is_template")
    public Boolean isTemplate;
    @JsonProperty("off_hauler_ids")
    public Object offHaulerIds;
    @JsonProperty("barges_number")
    public List<BargesNumber> bargesNumber;
    @JsonProperty("daily_limits")
    public List<DailyLimit> dailyLimits;
}
