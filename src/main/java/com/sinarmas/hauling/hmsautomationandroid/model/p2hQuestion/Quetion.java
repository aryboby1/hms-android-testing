package com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Quetion {

    @JsonProperty("category")
    public String category;
    @JsonProperty("quetion_number")
    public Integer quetionNumber;
    @JsonProperty("quetion")
    public String quetion;
}
