package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DetailResponse {

    @JsonProperty("dailyLimitId")
    public Integer dailyLimitId;
    @JsonProperty("route")
    public Route route;
    @JsonProperty("isTemplate")
    public Boolean isTemplate;
    @JsonProperty("isDone")
    public Boolean isDone;
    @JsonProperty("workPartner")
    public List<WorkPartner> workPartner;
}
