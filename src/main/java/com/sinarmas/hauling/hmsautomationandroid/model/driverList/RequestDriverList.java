package com.sinarmas.hauling.hmsautomationandroid.model.driverList;

import lombok.Data;

@Data
public class RequestDriverList {

    public Filter filter;
    public Integer limit;
    public Integer page;
    public String sort;
    public Boolean ascending;

    public RequestDriverList(String attendanceApprovalStatus){
        this.filter.attendanceApprovalStatus = attendanceApprovalStatus;
    }

}
