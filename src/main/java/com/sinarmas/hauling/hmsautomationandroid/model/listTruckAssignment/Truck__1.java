package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Truck__1 {

    @JsonProperty("id")
    public Integer id;
    @JsonProperty("ucanTruckId")
    public Integer ucanTruckId;
    @JsonProperty("noLambung")
    public String noLambung;
    @JsonProperty("haulingCompany")
    public String haulingCompany;
    @JsonProperty("tare")
    public Integer tare;
    @JsonProperty("totalGandar")
    public Integer totalGandar;
    @JsonProperty("truckRouteStatus")
    public String truckRouteStatus;
    @JsonProperty("truckRouteNotes")
    public Object truckRouteNotes;
    @JsonProperty("selected")
    public Object selected;
}
