package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Route  {

    @JsonProperty("routeId")
    public Integer routeId;
    @JsonProperty("romPitId")
    public Integer romPitId;
    @JsonProperty("romPitName")
    public String romPitName;
    @JsonProperty("romPitType")
    public String romPitType;
    @JsonProperty("portId")
    public Integer portId;
    @JsonProperty("portName")
    public String portName;
    @JsonProperty("originName")
    public Object originName;
    @JsonProperty("isWim")
    public Boolean isWim;
    @JsonProperty("priority")
    public Integer priority;
    @JsonProperty("isDeleted")
    public Integer isDeleted;
    @JsonProperty("createdDate")
    public String createdDate;
    @JsonProperty("modifiedDate")
    public String modifiedDate;
    @JsonProperty("distanceInKM")
    public Object distanceInKM;
    @JsonProperty("hibernateLazyInitializer")
    public HibernateLazyInitializer hibernateLazyInitializer;
}
