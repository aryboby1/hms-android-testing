package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class WorkPartner {

    
@JsonProperty("work_partner_id")
public Integer workPartnerId;
@JsonProperty("name")
public String name;
@JsonProperty("code")
public String code;
@JsonProperty("cnt_trucks")
public Integer cntTrucks;
@JsonProperty("is_deleted")
public Object isDeleted;
@JsonProperty("created_date")
public String createdDate;
@JsonProperty("modified_date")
public String modifiedDate;
@JsonProperty("selected")
public Boolean selected;
@JsonProperty("selected_truck_ids")
public List<Object> selectedTruckIds;
@JsonProperty("truck_list")
public List<Truck> truckList;
}
