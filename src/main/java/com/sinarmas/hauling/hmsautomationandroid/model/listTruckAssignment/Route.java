package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Route {

    @JsonProperty("isWim")
    public Boolean isWim;
    @JsonProperty("originName")
    public Object originName;
    @JsonProperty("portId")
    public Integer portId;
    @JsonProperty("portName")
    public String portName;
    @JsonProperty("priority")
    public Integer priority;
    @JsonProperty("romPitId")
    public Integer romPitId;
    @JsonProperty("romPitName")
    public String romPitName;
    @JsonProperty("romPitType")
    public String romPitType;
    @JsonProperty("routeId")
    public Integer routeId;
}
