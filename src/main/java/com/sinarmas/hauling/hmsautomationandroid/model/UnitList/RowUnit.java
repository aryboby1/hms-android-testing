package com.sinarmas.hauling.hmsautomationandroid.model.UnitList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RowUnit {

    @JsonProperty("uuid")
    public String uuid;
    @JsonProperty("truckAssignmentId")
    public Integer truckAssignmentId;
    @JsonProperty("truckId")
    public Integer truckId;
    @JsonProperty("ucanTruckId")
    public Integer ucanTruckId;
    @JsonProperty("totalGandar")
    public Integer totalGandar;
    @JsonProperty("tareWeightSum")
    public Object tareWeightSum;
    @JsonProperty("grossWeightSum")
    public Object grossWeightSum;
    @JsonProperty("netWeightSum")
    public Object netWeightSum;
    @JsonProperty("coalType")
    public Object coalType;
    @JsonProperty("shiftId")
    public Integer shiftId;
    @JsonProperty("shiftCode")
    public Integer shiftCode;
    @JsonProperty("shiftName")
    public String shiftName;
    @JsonProperty("isDeleted")
    public Integer isDeleted;
    @JsonProperty("isTemplate")
    public Boolean isTemplate;
    @JsonProperty("noLambung")
    public String noLambung;
    @JsonProperty("expired")
    public String expired;
    @JsonProperty("rom")
    public String rom;
    @JsonProperty("portName")
    public String portName;
    @JsonProperty("driverName")
    public Object driverName;
    @JsonProperty("driverItwsId")
    public Object driverItwsId;
    @JsonProperty("driverIsafeNumber")
    public Object driverIsafeNumber;
    @JsonProperty("truckStatus")
    public String truckStatus;
    @JsonProperty("truckTypeName")
    public String truckTypeName;
    @JsonProperty("shiftDate")
    public String shiftDate;
    @JsonProperty("portId")
    public Integer portId;
    @JsonProperty("romPitId")
    public Integer romPitId;
    @JsonProperty("pitStockId")
    public Object pitStockId;
    @JsonProperty("pitStockName")
    public Object pitStockName;
    @JsonProperty("company")
    public String company;
    @JsonProperty("tripStatus")
    public Integer tripStatus;
    @JsonProperty("canBeRerouted")
    public Boolean canBeRerouted;
    @JsonProperty("isRerouted")
    public Boolean isRerouted;
    @JsonProperty("totalRitase")
    public Integer totalRitase;
    @JsonProperty("activeUnitTripId")
    public Object activeUnitTripId;
    @JsonProperty("sosId")
    public Object sosId;
    @JsonProperty("sosStatus")
    public Object sosStatus;
    @JsonProperty("sosType")
    public Object sosType;
    @JsonProperty("sosCreatedAt")
    public Object sosCreatedAt;
}
