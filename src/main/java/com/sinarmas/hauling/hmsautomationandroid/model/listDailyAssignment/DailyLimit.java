package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DailyLimit  {

    @JsonProperty("id")
    public Integer id;
    @JsonProperty("type")
    public String type;
    @JsonProperty("route")
    public Route route;
    @JsonProperty("fleet_quota")
    public Integer fleetQuota;
    @JsonProperty("rom_quota")
    public Integer romQuota;
    @JsonProperty("pit_stock_origin")
    public Object pitStockOrigin;
    @JsonProperty("truck_limits")
    public List<TruckLimit> truckLimits;
    @JsonProperty("fleet_limits")
    public List<FleetLimit> fleetLimits;
    @JsonProperty("shift_tonnage")
    public Float shiftTonnage;
    @JsonProperty("route_tonnage")
    public Float routeTonnage;
}
