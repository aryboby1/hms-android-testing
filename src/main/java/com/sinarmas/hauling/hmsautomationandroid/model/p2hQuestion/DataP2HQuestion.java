package com.sinarmas.hauling.hmsautomationandroid.model.p2hQuestion;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DataP2HQuestion {

    @JsonProperty("quetions")
    public List<Quetion> quetions;
}
