package com.sinarmas.hauling.hmsautomationandroid.model.listDailyAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class BargesNumber  {

@JsonProperty("key")
public String key;
@JsonProperty("name")
public String name;
@JsonProperty("value")
public String value;
@JsonProperty("description")
public String description;
}
