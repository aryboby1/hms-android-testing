package com.sinarmas.hauling.hmsautomationandroid.model.UnitList;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseUnitList {
    @JsonProperty("code")
    public String code;
    @JsonProperty("message")
    public String message;
    @JsonProperty("data")
    public DataUnit data;
}
