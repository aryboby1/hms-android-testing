package com.sinarmas.hauling.hmsautomationandroid.model.driverList;

import lombok.Data;

@Data
public class Filter {

    public String kontraktor;
    public String driverName;
    public String shiftDate;
    public String workPreparationStatus;
    public String tripStatus;
    public Integer routeId;
    public Boolean hasSos;
    public String attendanceApprovalStatus;
    
}
