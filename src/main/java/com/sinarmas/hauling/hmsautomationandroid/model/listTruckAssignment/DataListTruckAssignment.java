package com.sinarmas.hauling.hmsautomationandroid.model.listTruckAssignment;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class DataListTruckAssignment {

    @JsonProperty("date")
    public String date;
    @JsonProperty("shiftResponse")
    public ShiftResponse shiftResponse;
    @JsonProperty("detailResponses")
    public List<DetailResponse> detailResponses;
}
