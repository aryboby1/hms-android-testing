package com.sinarmas.hauling.hmsautomationandroid.data;

public final class UrlConstant {
    private UrlConstant() {
    }

    public static String baseUrl = "https://dev-hms.ugems.id";

    public static final String getApiPath(String url) {
        String result = "";
        switch (url) {
            case "Reset_ALL_TRIP":
                result = "/api/v1/test/reset-all-trip";
                break;
            case "LOGIN":
                result = "/api/v1/auth/authenticate";
                break;
            case "LOGIN_ADMIN":
                result = "/api/v1/auth/admin/authenticate";
                break;
            case "GET_QUESTION_FIT_TO_WORK":
                result = "/api/v1/work-preparation/attendance-quetion";
                break;
            case "SEND_FIT_TO_WORK":
                result = "/api/v1/work-preparation/attendance-v2";
                break;
            case "SUBMIT_FIT_TO_WORK":
                result = "/api/v1/work-preparation/attendance-score-v2";
                break;
            case "GET_DRIVER_LIST":
                result = "/api/v1/webapp/driver-list";
                break;
            case "APPROVAL_FIT_TO_WORK":
                result = "/api/v1/webapp/approve-attendance";
                break;
            case "GET_UNIT_LIST":
                result = "/api/v1/work-preparation/unit-list-v2";
                break;
            case "SELECT_UNIT":
                result = "/api/v1/work-preparation/select-unit";
                break;
            case "GET_P2H_QUESTION":
                result = "/api/v1/work-preparation/p2h-quetions";
                break;
            case "SUBMIT_P2H_QUESTION":
                result = "/api/v1/work-preparation/p2h-submit";
                break;
            case "SUBMIT_DAILY_ASSIGNMENT":
                result = "/api/v1/webapp/daily_limit/create";
                break;
            case "GET_DAILY_ASSIGNMENT":
                result = "/api/v1/webapp/daily_limit/detail";
                break;
            case "SUBMIT_TRUCK_ASSIGNMENT":
                result = "/api/v1/webapp/truck_assignment/create";
                break;
            case "GET_Truck_ASSIGNMENT":
                result = "/api/v1/webapp/truck_assignment/detail";
                break;
            case "RESPONSE_P2H":
                result = "/api/v1/webapp/approve-p2h";
                break;
        }
        return result;
    }
}
