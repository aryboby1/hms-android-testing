package com.sinarmas.hauling.hmsautomationandroid.data;

public final class WordingConstant {
    private WordingConstant() {
    }

    public static final String getTitleMenu(String menu) {
        String result = "";
        switch (menu) {
            case "MASTER_OBU":
                result = "On Boarding Unit (OBU)";
                break;
            case "PORT_DATA":
                result = "Manage Port";
                break;
            case "TRUCK_DATA":
                result = "Manage Port";
                break;
            case "ADMIN":
                result = "Management Admin";
                break;
            case "Driver":
                result = "Driver";
                break;
        }
        return result;
    }

    public static final String getMenu(String menu) {
        String result = "";
        switch (menu) {
            case "MASTER_OBU":
                result = "Onboarding Unit (OBU)";
                break;
            case "PORT_DATA":
                result = "PORT Data";
                break;
            case "TRUCK_DATA":
                result = "Truk Data";
                break;
            case "ADMIN":
                result = "Pegawai (Admin)";
                break;
            case "Driver":
                result = "Driver";
                break;
        }
        return result;
    }

    public static String getURLMenu(String menu) {
        String result = "";
        switch (menu) {
            case "MASTER_OBU":
                result = "Onboarding Unit (OBU)";
                break;
            case "PORT_DATA":
                result = "PORT Data";
                break;
            case "TRUCK_DATA":
                result = "https://dev-hms.ugems.id/trucks";
                break;
            case "ADMIN":
                result = "Pegawai (Admin)";
                break;
            case "Driver":
                result = "https://dev-hms.ugems.id/drivers";
                break;
        }
        return result;
    }

    public static String truckAssignmentReq(Integer dailyLimitId, String date, String shiftOpt, String routeId) {
        // 2023-12-21
        return "[ \n" +
                "  { \n" +
                "    \"date\": \"" + date + "\", \n" +
                "    \"shift_id\": " + shiftOpt + ", \n" +
                "    \"route_id\": " + routeId + ", \n" +
                "    \"is_template\": false, \n" +
                "    \"work_partner_id\": 167, \n" +
                "    \"truck_routes\": [ \n" +
                "      { \n" +
                "        \"truck_id\": 2085, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      }, \n" +
                "      { \n" +
                "        \"truck_id\": 2120, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      }, \n" +
                "      { \n" +
                "        \"truck_id\": 2121, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      }, \n" +
                "      { \n" +
                "        \"truck_id\": 2127, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      }, \n" +
                "      { \n" +
                "        \"truck_id\": 2128, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      }, \n" +
                "      { \n" +
                "        \"truck_id\": 2990, \n" +
                "        \"route_id\": " + routeId + ", \n" +
                "        \"truck_route_status\": \"READY\", \n" +
                "        \"truck_route_notes\": \"string\" \n" +
                "      } \n" +
                "    ] \n" +
                "  } \n" +
                "]";
    }

    public static String approveP2HReq(Integer reference_id, Boolean isApprove, String randomAlphanumeric) {
        // 2023-12-21
        return "  { \n" +
                "    \"trip_id\": " + reference_id + ", \n" +
                "    \"is_approve\": " + isApprove + ", \n" +
                "    \"reason\": \"" + randomAlphanumeric + "\" \n" +
                "  } \n";
    }
}
