package com.sinarmas.hauling.hmsautomationandroid.data;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class CommonData {
    
    private String accessTokenAdmin;
    private String accessTokenUser;
}
