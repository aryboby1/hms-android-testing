package com.sinarmas.hauling.hmsautomationandroid.data;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component("com.sinarmas.hauling.hmsautomation.data.MasterDataData")
public class MasterDataData {

    private String noDeviceObuADD;

    private List<String> roleTypeList = Arrays.asList(
            "ADMIN",
            "FMSVENDORSUPERVISOR",
            "MECHANICALSUPERVISOR",
            "OPHAULINGBIB"

    );

    public static String getEmailUser(String user){
        String result = "";
        switch(user){
            case "Noor":
            result = "b59119";
            break;
            case "AdminDwiki":
            result = "mas_dwiki_testing@mail.com";
            break;
            case "Kinanto":
            result = "b13000";
            break;
            case "Testing Driver":
            result = "B11122";
            break;
            case "WIRADAT":
            result = "cd45ea";
            break;
        }
        return result;
    }

    public static String getPassUser(String user) {
        String result = "";
        switch(user){
            case "Noor":
            result = "b59119";
            break;
            case "AdminDwiki":
            result = "Welcome@1";
            break;
            default:
            result = "12345678";
        }
        return result;
    }

}
